define(function() {
    var Class = function() {};

    var extend = function (instProps, statProps) {
        var Child = function() {
            this._super = new this._super(this);

            if(this._init)
            this._init.apply(this, arguments);
        };

        var proto = this.prototype,
            childProto = Child.prototype,
            superProto = {},
            prop;

        for(prop in proto) {
            childProto[prop] = proto[prop];
        }

        for(prop in instProps) {
            childProto[prop] = instProps[prop];

            if(typeof proto[prop] === 'function') {
                superProto[prop] = (function(method) {
                    return function() {
                        return method.apply(this.__self, arguments);
                    };
                })(proto[prop]);
            }
        }

        for(prop in statProps) {
            Child[prop] = statProps[prop];
        }

        childProto._class = Child;
        (childProto._super = function(self) {
            this.__self = self;
        }).prototype = superProto;

        Child._extend = extend;
        Child._base = this;
        return Child;
    };

    Class._extend = extend;
    return Class;
});
